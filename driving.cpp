#include "motor.h"
#include "Arduino.h"

#define BASE_SPEED '3'

// Base method for actually sending commands to the motors.
// Using this method stops us from having to duplicate the delay everywhere
void drive_motor(Motor left, Motor right,
    char speed1, char speed2)
{
  left.Drive(FORWARD, speed1);
  delay(10);
  right.Drive(REVERSE, speed2);
}

void straight(Motor left, Motor right)
{
  drive_motor(left, right, BASE_SPEED, BASE_SPEED);
}

void left_slight(Motor left, Motor right)
{
  drive_motor(left, right, '0', '3');
}

void right_slight(Motor left, Motor right)
{
  drive_motor(left, right, '3', '0');
}

void left_full(Motor left, Motor right)
{
  left.Drive(REVERSE, '2');
  delay(10);
  right.Drive(REVERSE, '3');
}

void right_full(Motor left, Motor right)
{
  left.Drive(FORWARD, '3');
  delay(10);
  right.Drive(FORWARD, '2');
}
