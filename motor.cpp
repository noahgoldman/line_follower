#include "assert.h"
#include "motor.h"
#include "Arduino.h"

Motor::Motor(char id) : id(id) {}

void Motor::Drive(int direction, char speed) 
{
  Serial.print(this->id);

  Serial.print((direction == FORWARD) ? 'F' : 'R');

  Serial.print(speed);

  Serial.print('\r');
}
