class Motor;

void drive_motor(Motor left, Motor right, char speed1, char speed2);
void straight(Motor left, Motor right);
void left_slight(Motor left, Motor right);
void right_slight(Motor left, Motor right);
void left_full(Motor left, Motor right);
void right_full(Motor left, Motor right);
