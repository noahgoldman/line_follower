#ifndef IR_H_
#define IR_H_


class IR {
  public:
    explicit IR( int pin);
    inline bool Line() 
    {
      return (this->Read() > this->threshold);
    }

    int Read();
    int threshold;
    int pin;
};

#endif // IR_H_
