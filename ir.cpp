#include "ir.h"
#include "Arduino.h"
IR::IR(int sensor_pin) : pin(sensor_pin) {}

int IR::Read() 
{
  return analogRead(this->pin);
}
