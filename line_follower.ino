#include "Arduino.h"
#include "motor.h"
#include "ir.h"
#include "driving.h"

#define LEFT 1
#define RIGHT 2
#define CLEAR_TIME 50

//#define DEBUG

IR ir_center(A0);
IR ir_LC(A1);
IR ir_RC(A2);
IR ir_left(A3);
IR ir_right(A4);
int led = 13;
int edge_flag = 0;
unsigned long time = 0;

// Motors
Motor left('1');
Motor right('2');

inline int average(int i1, int i2)
{
  return (i1 + i2) / 2;
}

//place car on white paper, wait for led to blink, wait 5 secs, after the second blink, move
//car to black paper. wait for blink. car then waits for 10 seconds before starting 
void setup() {         
#ifdef DEBUG
  Serial.begin(9600);
#else
  Serial.begin(115200);
#endif

  pinMode(led,OUTPUT);
  delay(1000);
  digitalWrite(led,HIGH);
  delay(250);
  digitalWrite(led,LOW);
  delay(250);
  digitalWrite(led,HIGH);
  delay(5000);
  
  int iterations = 15;
  int center = 0, lc = 0, rc = 0, left = 0, right = 0;
  for(int i = 0; i < iterations; i++){
    center += ir_center.Read();
    lc += ir_LC.Read();
    rc += ir_RC.Read();
    left += ir_left.Read();
    right += ir_right.Read();
    delay(1);
  }
  center /= iterations;
  lc /= iterations;
  rc /= iterations;
  left /= iterations;
  right /= iterations;

  pinMode(led,OUTPUT);
  delay(1000);
  digitalWrite(led,HIGH);
  delay(250);
  digitalWrite(led,LOW);
  delay(250);
  digitalWrite(led,HIGH);
  delay(5000);

  int b_center = 0, b_lc = 0, b_rc = 0, b_left = 0, b_right = 0;
  for(int i = 0; i < iterations; i++){
    b_center += ir_center.Read();
    b_lc += ir_LC.Read();
    b_rc += ir_RC.Read();
    b_left += ir_left.Read();
    b_right += ir_right.Read();
    delay(1);
  }
  b_center /= iterations;
  b_lc /= iterations;
  b_rc /= iterations;
  b_left /= iterations;
  b_right /= iterations;

  ir_center.threshold = average(b_center, center);
  ir_LC.threshold = average(b_lc, lc);
  ir_RC.threshold = average(b_rc, rc);
  ir_left.threshold = average(b_left, left);
  ir_right.threshold = average(b_right, right);

  digitalWrite(led,LOW);

#ifdef DEBUG
  Serial.println(ir_center.threshold);
  Serial.println(ir_LC.threshold);
  Serial.println(ir_RC.threshold);
  Serial.println(ir_left.threshold);
  Serial.println(ir_right.threshold);
#endif
  delay(10000);
}

void loop() {
  ////////////////
  // MOTOR TEST //
  ////////////////
  /*
  left_full(left, right);
  delay(500);
  */

  ////////////////////
  // IR SENSOR TEST //
  ////////////////////
  /*
  Serial.println(ir_LC.Line());
  Serial.println(ir_LC.Read());
  Serial.println(ir_RC.Line());
  Serial.println(ir_RC.Read());
  Serial.println(ir_center.Line());
  Serial.println(ir_center.Read());
  Serial.println(ir_left.Line());
  Serial.println(ir_left.Read());
  Serial.println(ir_right.Line());
  Serial.println(ir_right.Read());
  Serial.println();
  delay(1000);
  */
  
  center_case:
  
  delay(1);
  if(time + CLEAR_TIME < millis()){
    edge_flag = 0;
#ifdef DEBUG
    Serial.println("FLAG CLEARED");
#endif
  }
  
  int Left = ir_left.Line();
  if(Left != ir_right.Line()){
    if(Left){
      edge_flag = LEFT;
    }
    else{
      edge_flag = RIGHT;
    }
    time = millis();
#ifdef DEBUG
    Serial.print("edge flag is set to ");
    Serial.println(edge_flag);
#endif
  }
  
  //case x000x
  delay(1);
  if(!ir_center.Line() && !ir_RC.Line() && !ir_LC.Line()){
    goto edge_case;
  }
  
  //case xaxax
  delay(1);
  if(ir_RC.Line() == ir_LC.Line()){
    //go straight
    //right and left motors same speed
#ifdef DEBUG
    Serial.println("driving striaght");
#else
    straight(left, right);
#endif
  }
  
  //case x1x0x
  delay(1);
  if(!ir_RC.Line() && ir_LC.Line()){
    //turn left
    //right motor 30% faster than left motor
#ifdef DEBUG
    Serial.println("turning left slightly");
#else
    left_slight(left, right);
#endif
  }
  
  //case x0x1x
  delay(1);
  if(ir_RC.Line() && !ir_LC.Line()){
    //turn right
    //left motor 30% faster than right motor
#ifdef DEBUG
    Serial.println("turning right slightly");
#else
    right_slight(left, right);
#endif
  }
  
  goto center_case;
  
  edge_case:
  
  //case 1xxxx
  if(edge_flag == RIGHT){
    //hard turn right
#ifdef DEBUG
    Serial.println("turning hard right");
#else
    while(!ir_center.Line() && !ir_RC.Line() && !ir_LC.Line()){
      right_full(left, right);
      delay(1);
    }
#endif
  }
  
  //case xxxx1
  else if(edge_flag == LEFT){
    //hard turn left
#ifdef DEBUG
    Serial.println("turning hard left");
#else
    while(!ir_center.Line() && !ir_RC.Line() && !ir_LC.Line()){
      left_full(left, right);
      delay(1);
    }
#endif
  } 
  else {
    straight(left, right);
  }

  goto center_case;
}
