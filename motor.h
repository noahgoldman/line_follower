#ifndef MOTOR_H_
#define MOTOR_H_

#define FORWARD 1
#define REVERSE 0

class Motor {
  public:
    Motor(char id);
    void Drive(int direction, char speed) ;
    char id;
};

#endif // MOTOR_H_
